module elon-musk

go 1.16

require (
	github.com/adam-lavrik/go-imath v0.0.0-20210910152346-265a42a96f0b
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/johejo/golang-migrate-extra v0.0.0-20211005021153-c17dd75f8b4a
)
