package db

import (
	"database/sql"
	"embed"
	"log"

	"elon-musk/config"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source"
	"github.com/johejo/golang-migrate-extra/source/iofs"
)

//go:embed migrations/*.sql
var postgresFiles embed.FS

func MigrateDatabase(db *sql.DB, cfg *config.Config) {
	log.Print("checking database migrations")

	databaseDriver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		log.Printf("unable to create migration instance from database, err - %s", err.Error())
	}

	var d source.Driver
	d, err = iofs.New(postgresFiles, "migrations")
	if err != nil {
		log.Printf("failed to load migration files from source driver, err - %s", err.Error())
	}

	m, err := migrate.NewWithInstance("iofs", d, cfg.Name, databaseDriver)
	if err != nil {
		log.Printf("failed to initialise migration with postgres instance, err - %s", err.Error())
	}

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		log.Printf("failed to migrate database, err - %s", err.Error())
	}

	log.Print("database is up-to-date, all migrations applied")
}
