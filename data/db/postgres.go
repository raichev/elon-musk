package db

import (
	"database/sql"
)

func MakeDBConnection(psqlConnectionString string) (*sql.DB, error) {
	db, err := sql.Open("postgres", psqlConnectionString)
	if err != nil {
		return nil, err
	}

	return db, nil
}
