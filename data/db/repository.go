package db

func GetCreateORUpdateDataString() string {
	return `INSERT INTO tweets (id, conversation_id, created_at, date, replies_count, retweets_count, likes_count, tweet)
		VALUES($1, $2, $3, $4, $5, $6, $7, $8)
		ON CONFLICT (id) DO UPDATE
		SET id = EXCLUDED.id,
		 conversation_id = EXCLUDED.conversation_id,
		 created_at = EXCLUDED.created_at,
		 date = EXCLUDED.date,
		 replies_count = EXCLUDED.replies_count,
		 retweets_count = EXCLUDED.retweets_count,
		 likes_count = EXCLUDED.likes_count,
		 tweet = EXCLUDED.tweet`
}

func GetTweetsPerDayString() string {
	return `SELECT EXTRACT(year FROM date) AS year, COUNT(*) AS count FROM tweets GROUP BY EXTRACT(year FROM date)`
}

func GetRetweetsPerDayString() string {
	return `SELECT EXTRACT(year FROM date) AS year, SUM(retweets_count) AS count FROM tweets GROUP BY EXTRACT(year FROM date)`
}

func GetLikesPerDayQueryString() string {
	return `SELECT EXTRACT(year FROM date) AS year, SUM(likes_count) AS count FROM tweets GROUP BY EXTRACT(year FROM date)`
}

func GetRepliesPerDayQueryString() string {
	return `SELECT EXTRACT(year FROM date) AS year, SUM(replies_count) AS count FROM tweets GROUP BY EXTRACT(year FROM date)`
}

func GetActivityPerHourQueryString() string {
	return `SELECT EXTRACT(hour FROM date) AS hour, COUNT(*) AS count FROM tweets GROUP BY EXTRACT(hour FROM date)`
}
