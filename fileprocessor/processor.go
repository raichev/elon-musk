package fileprocessor

import (
	"bufio"
	"database/sql"
	database "elon-musk/data/db"
	"encoding/json"
	"os"
)

type Tweet struct {
	Id             int64  `json:"id"`
	ConversationId string `json:"conversation_id"`
	CreatedAt      int64  `json:"created_at"`
	Date           string `json:"date"`
	Time           string `json:"time"`
	RepliesCount   int32  `json:"replies_count"`
	RetweetCount   int32  `json:"retweets_count"`
	LikesCount     int32  `json:"likes_count"`
	Tweet          string `json:"tweet"`
}

func ManageJsonFile(db *sql.DB) error {
	file, err := os.Open("elonmusk.json")
	if err != nil {
		return err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	q := database.GetCreateORUpdateDataString()
	for scanner.Scan() {
		tmp := scanner.Bytes()
		var data = &Tweet{}
		err = json.Unmarshal(tmp, data)
		if err != nil {
			return err
		}
		_, err := db.Exec(q, data.Id, data.ConversationId, data.CreatedAt, data.Date+" "+data.Time, data.RepliesCount, data.RetweetCount, data.LikesCount, data.Tweet)
		if err != nil {
			return err
		}
	}

	return nil
}
