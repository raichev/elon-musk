package main

import (
	"elon-musk/config"
	database "elon-musk/data/db"
	"elon-musk/fileprocessor"
	"elon-musk/handler"
	"fmt"
	"html"
	"net"
	"net/http"
)

func main() {
	fmt.Println("Starting processing...")
	cfg := config.NewConfig()
	db, err := database.MakeDBConnection(cfg.GetDBConnectionString())
	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()

	database.MigrateDatabase(db, cfg)
	err = fileprocessor.ManageJsonFile(db)
	if err != nil {
		fmt.Println(err)
	}
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}
	port := listener.Addr().(*net.TCPAddr).Port
	fmt.Printf("Please access: http://localhost:%d", port)
	htmlString, err := handler.GetTweetsPerYear(db, database.GetTweetsPerDayString())
	if err != nil {
		fmt.Println(err)
	}

	htmlStringRetweets, err := handler.GetRetweetsPerYear(db, database.GetRetweetsPerDayString())
	if err != nil {
		fmt.Println(err)
	}

	htmlStringLikes, err := handler.GetLikesPerYear(db, database.GetLikesPerDayQueryString())
	if err != nil {
		fmt.Println(err)
	}

	htmlStringReplies, err := handler.GetRepliesPerYear(db, database.GetRepliesPerDayQueryString())
	if err != nil {
		fmt.Println(err)
	}

	htmlStringActivity, err := handler.GetActivityPerHour(db, database.GetActivityPerHourQueryString())
	if err != nil {
		fmt.Println(err)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, htmlString, html.EscapeString(r.URL.Path))
	})
	http.HandleFunc("/retweets", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, htmlStringRetweets, html.EscapeString(r.URL.Path))
	})
	http.HandleFunc("/likes", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, htmlStringLikes, html.EscapeString(r.URL.Path))
	})
	http.HandleFunc("/replies", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, htmlStringReplies, html.EscapeString(r.URL.Path))
	})
	http.HandleFunc("/activity", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, htmlStringActivity, html.EscapeString(r.URL.Path))
	})

	http.Serve(listener, nil)
}
