package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

var data Config

type Config struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

func (c Config) GetDBConnectionString() string {
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		c.Host,
		c.Port,
		c.User,
		c.Password,
		c.Name,
	)
}

func getConfigurationData() (*Config, error) {
	confFile, err := os.Open("config.json")
	if err != nil {
		return nil, err
	}
	defer confFile.Close()
	conf, err := ioutil.ReadAll(confFile)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(conf, &data)
	if err != nil {
		return nil, err
	}

	return &data, nil
}

func NewConfig() *Config {
	data, err := getConfigurationData()
	if err != nil {
		fmt.Println(err)
		panic("config not set")
	}

	return data
}
