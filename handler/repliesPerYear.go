package handler

import (
	"database/sql"
	"fmt"
	"github.com/adam-lavrik/go-imath/ix"
	"strconv"
	"strings"
)

type repliesPerDay struct {
	date         int
	countReplies int
}

func GetRepliesPerYear(db *sql.DB, string string) (string, error) {
	rows, err := db.Query(string)
	if err != nil {
		return "", err
	}
	defer rows.Close()
	var date []int
	var counts []int
	for rows.Next() {
		reply := repliesPerDay{}
		if err := rows.Scan(&reply.date, &reply.countReplies); err != nil {
			return "nil, nil", err
		}
		date = append(date, reply.date)
		counts = append(counts, reply.countReplies)
	}
	countsSeparate := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(counts)), ","), "[]")
	minDate := ix.MinSlice(date)
	maxDate := ix.MaxSlice(date)
	return `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Chars</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <style>
        ul.nav {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        ul.nav li {
            float: left;
        }

        ul.nav li a {
            display: block;
            color: #f2f2f2;
            text-align: left;
            padding: 14px 16px;
            text-decoration: none;
        }

        ul.nav li a:hover {
            background-color: #aae5d8;
            color: black;
        }

        ul.nav a.active {
            background-color: #aae5d8;
            color: black;
        }
        .highcharts-figure,
        .highcharts-data-table table {
            min-width: 360px;
            max-width: 800px;
            margin: 1em auto;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #ebebeb;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

    </style>
</head>
<body>
<div class="container">
    <ul class="nav">
        <li><a href="/">Tweets per year</a></li>
        <li><a href="/retweets">Retweets per year</a></li>
        <li><a href="/likes">Likes per year</a></li>
        <li><a href="/replies">Replies per year</a></li>
        <li><a href="/activity">Activity per hour</a></li>
    </ul>
    <figure class="highcharts-figure">
        <div id="container"></div>
        <p class="highcharts-description">
        </p>
    </figure>
</div>
</body>
<script>
    Highcharts.chart('container', {

        title: {
            text: 'Charts for how many replies we have every year for all period'
        },


        yAxis: {
            title: {
                text: 'Replies'
            }
        },

        xAxis: {
            accessibility: {
                rangeDescription: 'Range: ` + strconv.Itoa(minDate) + ` to ` + strconv.Itoa(maxDate) + `'
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: ` + strconv.Itoa(minDate) + `
            }
        },

        series: [{
            name: 'Replies per year',
            data: [` + countsSeparate + `]
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
</script>
</html>
`, nil
}
