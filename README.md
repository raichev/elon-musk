# Graph Elon Musk

This is a small exercise for making some Charts with the activity in social network of Elon Musk and the count tweets/retweets/likes/replies per year 
## Development Environment

### Getting Started

#### Setup

1. Create local DB with the needed credentials - `host`, `username`, `password`, `port`
2. Copy `config.json.dist` to `config.json` with the data from previous step 
3. Run `go run main.go`
   1. Example output into command line
      1. Starting processing...
         
         2021/12/21 06:38:15 checking database migrations
      
         2021/12/21 06:38:15 database is up-to-date, all migrations applied
      
         Please access: http://localhost:49536
      
    `Port` is gettig automatically from your local machine
      
4. Open the url which is shown in command line 

### Available URLS
All pages have menu from where you can switch between different charts
1. http://localhost:`PORT`/ - `Chars for how many tweets we have every year for all period`
2. http://localhost:`PORT`/retweets - `Chars for how many retweets we have every year for all period`
3. http://localhost:`PORT`/likes - `Chars for how many likes we have every year for all period`
4. http://localhost:`PORT`/replies - `Chars for how many replies we have every year for all period`
4. http://localhost:`PORT`/activity - `Chars for in which time Elon Musk is more active`
# License

Copyright 2021, Martin Ivanov
